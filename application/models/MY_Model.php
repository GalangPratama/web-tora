<?php

class MY_Model extends CI_Model{

   public function __construct() {
    
    parent::__construct();
    $this->load->database();
    
  }

  // menampilkan semua data dari sebuah tabel.
  public function getAll($tables){
    return $this->db->get($tables);
  }

  // menampilkan semua data berdasarkan sebuah statement.
  public function getWhere($tables, $param, $value){
    $this->db->where($param,$value);
    return $this->db->get($tables);
  }

  // menampilkan limit data dari sebuah tabel.
  public function getLimit($tables, $limit){
    return $this->db->get($tables, $limit);
  }

     // menampilkan semua data dari sebuah tabel dengan order.
  public function getOrder($tables,$field,$sort){
    $this->db->order_by($field, $sort);
    return $this->db->get($tables);
  }

    //menampilkan satu record brdasarkan parameter.
  public  function getByID($tables,$pk,$id){
    $this->db->where($pk,$id);
    return $this->db->get($tables);
  }

    // memasukan data ke database.
  public function insert($tables,$data){
    $this->db->insert($tables,$data);
  }

    // update data kedalalam sebuah tabel
  public function update($tables,$data,$pk,$id){
    $this->db->where($pk,$id);
    $this->db->update($tables,$data);
  }

    // menghapus data dari sebuah tabel
  public function delete($tables,$pk,$id){
    $this->db->where($pk,$id);
    $this->db->delete($tables);
  }

    // menjumlahkan data dari sebuah tabel
  public function sum($tables,$field,$pk,$id){
    $query = $this->db->select_sum($field, 'Amount');
    $query = $this->db->where($pk,$id);
    $query = $this->db->get($tables);
    $result = $query->result();

    return $result[0]->Amount;
  }

  // menghitung rows data dari sebuah tabel
  public function numrows($tables){
    $this->db->select('*');
    $this->db->from($tables);
    $result = $this->db->get()->num_rows();
    return $result;
  }

  // menghitung rows data dari sebuah tabel
  public function numWhereIn($tables,$field,$statement){
    $this->db->select('*');
    $this->db->from($tables);
    $this->db->or_where_in($field,$statement);
    $result = $this->db->get()->num_rows();
    return $result;
  }

  

  public function join2($table1,$id1,$table2,$id2,$field,$sort) 
  {
   $this->db->select('*');
   $this->db->from($table1);
   $this->db->join($table2, $table2.'.'.$id2.'='.$table1.'.'.$id1);
   $this->db->order_by($field,$sort);
   $query = $this->db->get();
   return $query->result();
 }

 public function join2_where($table1,$id1,$table2,$id2,$param,$code) 
 {
   $this->db->select('*');
   $this->db->from($table1);
   $this->db->join($table2, $table2.'.'.$id2.'='.$table1.'.'.$id1);
   $this->db->where($param,$code);
   return $this->db->get();
 }

 public function _do_upload($directory,$data)
 {
        $config['upload_path']          = $directory;
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 0; //set max size allowed in Kilobyte
        $config['max_width']            = 0; // set max width image allowed
        $config['max_height']           = 0; // set max height allowed

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload($data)) //upload and validate
        {
          $error = array('error' => $this->upload->display_errors());
        }
        return $this->upload->data('file_name');
      }


      function unlink($tables,$pk,$id,$foto){
        $row = $this->db->where($pk,$id)->get($tables)->row();
        // Jika terdapat foto maka hapus
        if(file_exists('uploads/product/'.$row->$foto) && $row->$foto)
        {
          unlink('uploads/product/'.$row->$foto);
        }
        $this->db->where($pk, $id);
        $this->db->delete($tables);
        return true;      
      } 


    }

    ?>
