<!-- <section class="rev_slider_wrapper mt-xs-45">
    <div class="rev_slider materialize-slider">
        <ul>

            <li data-transition="fade" data-slotamount="default" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000" data-thumb="<?php echo base_url() ?>assets/img/slider/slide1.jpeg" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="materialize Material" data-description="">

                <img src="<?php echo base_url() ?>assets/img/slider/slide1.jpeg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina >

                <div class="tp-caption NotGeneric-Title1 tp-resizeme" data-x="['left','left','center','center']" data-hoffset="['20','20','0','0']" data-y="['center','center','top','top']" data-voffset="['-100','-100','50','50']" data-fontsize="['40']" data-lineheight="['70','60','40','40']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="800" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 5; color: #fff; white-space: nowrap;">TOTAL INTI CORPORA
                 </div>

                <div class="tp-caption tp-resizeme rev-subheading1" data-x="['left','left','center','center']" data-hoffset="['06','20','0','0']" data-y="['center','center','top','top']" data-voffset="['0','0','140','140']" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; color: #fff; white-space: nowrap;">known as a preferred quality software development <br> for logistics solutions
                </div>
                 <div class="button-learn animated fadeInDown slower">
                    <a href="#">LEARN MORE</a>
                </div>
         

            </li>

            <li data-transition="fade" data-slotamount="default" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000" data-thumb="<?php echo base_url() ?>assets/img/slider/slide2.jpeg" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="materialize Material" data-description="">

                <img src="<?php echo base_url() ?>assets/img/slider/slide2.jpeg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>

                <div class="tp-caption NotGeneric-Title2 tp-resizeme" data-x="['left','left','center','center']" data-hoffset="['20','20','0','0']" data-y="['center','center','top','top']" data-voffset="['-100','-100','50','50']" data-fontsize="['70','60','50','45']" data-lineheight="['70','60','40','40']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="800" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 5; color: #fff; white-space: nowrap;">PROVIDES A SPECIFIC PORTOFOLIO OF <br> INFORMATION TECHNOLOGY
                </div>

                <div class="tp-caption tp-resizeme rev-subheading2" data-x="['left','left','center','center']" data-hoffset="['20','20','0','0']" data-y="['center','center','top','top']" data-voffset="['0','0','140','140']" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; color: #fff; white-space: nowrap;"> <a href="#">THINK SYNERGY</a> 
                </div>

                </div>
            </li>

        </ul>
    </div>
</section> -->
<section>
<div class="slide">
    <div class="slide-image">
        <img src="<?php echo base_url() ?>assets/img/slider/slide1.jpeg">
    </div>
    <div class="slide-head">
        <h2 class="animated bounceInDown">We'r <span>TORA</span> </h2>
        <p class="animated bounceInUp">Known as a preferred quality software development <br> for logistics solutions</p>

        <a href="#" class="animated zoomIn">LEARN MORE</a>

    </div>
   
</div>
</section>

<div class="row" id="about-right">
    <div class="col-xs-12">
        <img src="<?php echo base_url() ?>assets/img/about-right.png">
        <div class="deskription">
            <div class="tittle-db-whoweare">
                <h2 class="mb-20">Who We Are</h2>
            </div>
            <div class="desc-db-whoweare">
                <p>PT. TOTAL INTI CORPORA provides a specific portfolio of information technology solutions and business process related to logistics and supply chain management. Our core portfolio comprises application development, business process consulting services as well as professional staffing services in information technology.</p>
                <a href="<?= base_url('dashboard/aboutus')?>">READ MORE</a>
            </div>
        </div>
    </div>
</div>

<section class="section-padding">
    <div class="container">
        <div class="text-center mb-40">
            <h2 class="section-title text-uppercase" style="color: #55ACEE;">What We Do</h2>
            <p class="section-sub">Quisque non erat mi. Etiam congue et augue sed tempus. Aenean sed ipsum luctus, scelerisque ipsum nec, iaculis justo. Sed at vestibulum purus, sit amet viverra diam nulla ac nisi rhoncus.</p>
        </div>
        <div class="seo-featured-carousel brand-dot">
            <div class="featured-item seo-service">
                <div class="icon">
                    <img class="img-responsive" src="assets/img/seo/service-1.jpg" alt="">
                </div>
                <div class="desc">
                    <h2>Web Application Development</h2>
                    <p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets</p>
                    <div class="bg-overlay"></div>
                    <p><a class="learn-more" href="#">Learn More <i class="fa fa-long-arrow-right"></i></a></p>
                </div>
            </div>
            <div class="featured-item seo-service">
                <div class="icon">
                    <img class="img-responsive" src="assets/img/seo/service-2.jpg" alt="">
                </div>
                <div class="desc">
                    <h2>Data Analysis</h2>
                    <p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets</p>
                    <div class="bg-overlay"></div>
                    <p><a class="learn-more" href="#">Learn More <i class="fa fa-long-arrow-right"></i></a></p>
                </div>
            </div>
            <div class="featured-item seo-service">
                <div class="icon">
                    <img class="img-responsive" src="assets/img/seo/service-3.jpg" alt="">
                </div>
                <div class="desc">
                    <h2>Custom Application Development</h2>
                    <p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets</p>
                    <div class="bg-overlay"></div>
                    <p><a class="learn-more" href="#">Learn More <i class="fa fa-long-arrow-right"></i></a></p>
                </div>
            </div>
            <div class="featured-item seo-service">
                <div class="icon">
                    <img class="img-responsive" src="assets/img/seo/service-4.jpg" alt="">
                </div>
                <div class="desc">
                    <h2>Content Marketing</h2>
                    <p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets</p>
                    <div class="bg-overlay"></div>
                    <p><a class="learn-more" href="#">Learn More <i class="fa fa-long-arrow-right"></i></a></p>
                </div>
            </div>
            <div class="featured-item seo-service">
                <div class="icon">
                    <img class="img-responsive" src="assets/img/seo/service-5.jpg" alt="">
                </div>
                <div class="desc">
                    <h2>Big Data</h2>
                    <p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets</p>
                    <div class="bg-overlay"></div>
                    <p><a class="learn-more" href="#">Learn More <i class="fa fa-long-arrow-right"></i></a></p>
                </div>
            </div>
            <div class="featured-item seo-service">
                <div class="icon">
                    <img class="img-responsive" src="assets/img/seo/service-6.jpg" alt="">
                </div>
                <div class="desc">
                    <h2>Application Mobile Development</h2>
                    <p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets</p>
                    <div class="bg-overlay"></div>
                    <p><a class="learn-more" href="#">Learn More <i class="fa fa-long-arrow-right"></i></a></p>
                </div>
            </div>
            <div class="featured-item seo-service">
                <div class="icon">
                    <img class="img-responsive" src="assets/img/seo/service-7.jpg" alt="">
                </div>
                <div class="desc">
                    <h2>Data Organize</h2>
                    <p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets</p>
                    <div class="bg-overlay"></div>
                    <p><a class="learn-more" href="#">Learn More <i class="fa fa-long-arrow-right"></i></a></p>
                </div>
            </div>
            <div class="featured-item seo-service">
                <div class="icon">
                    <img class="img-responsive" src="assets/img/seo/service-8.jpg" alt="">
                </div>
                <div class="desc">
                    <h2>Pay Per Click</h2>
                    <p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets</p>
                    <div class="bg-overlay"></div>
                    <p><a class="learn-more" href="#">Learn More <i class="fa fa-long-arrow-right"></i></a></p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-padding" id="get-now">
    <div class="container">
        <div class="promo-box gray-bg border-box">
            <div class="promo-info">
                <h2 class="text-extrabold text-uppercase font-25">Think Synergy</h2>
                <p>PT. Total Inti Corpora</p>
            </div>
            <div class="promo-btn">
                <a href="#." class="btn btn-lg text-capitalize waves-effect waves-light">Get it now</a>
            </div>
        </div>


    </div>
</section>
<section class="section-padding dark-bg lighten-4">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-7 light-grey-text">
                <h2 class="font-40 mb-30 white-text">Our Services</h2>
                <p>We offer a full range technology solutions and business process!</p>
                <ul class="list-icon mb-30">
                    <li><i class="material-icons">&#xE876;</i> Warehouse Management System</li>
                    <li><i class="material-icons">&#xE876;</i> Part Management System</li>
                    <li><i class="material-icons">&#xE876;</i> Courier Link System</li>
                    <li><i class="material-icons">&#xE876;</i> Human Resources Management</li>
                    <li><i class="material-icons">&#xE876;</i> Finance / Accounting Solutions</li>
                </ul>
            </div>
            <div class="col-xs-12 col-md-5 mt-sm-30">
                <img src="<?php echo base_url() ?>assets/img/services.png" alt="" class="img-responsive">
            </div>
        </div>
    </div>
</section>

<section class="section-padding" id="clients">
    <div class="container">
        <div class="text-center mb-30">
            <h2 class="section-title text-uppercase">Awesome clients</h2>
            <p class="section-sub">We are trusted by large and great company</p>
        </div>
        <div class="clients-grid gutter">
            <div class="row">
                <div class="col-xs-12 col-md-3 col-sm-6">
                    <div class="border-box">
                        <a href="#">
                            <img src="<?php echo base_url() ?>assets/img/client-logo/kurierid.jpg" alt="clients">
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3 col-sm-6">
                    <div class="border-box" >
                        <a href="#" id="total">
                            <img src="<?php echo base_url() ?>assets/img/client-logo/total.png" alt="clients">
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3 col-sm-6">
                    <div class="border-box">
                        <a href="#" id="kalbe">
                            <img src="<?php echo base_url() ?>assets/img/client-logo/kalbe.png" alt="clients">
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3 col-sm-6">
                    <div class="border-box">
                        <a href="#">
                            <img src="<?php echo base_url() ?>assets/img/client-logo/pda.png" alt="clients">
                        </a>
                    </div>
                </div>
                 <div class="col-xs-12 col-md-4 col-sm-6">
                    <div class="border-box">
                        <a href="#">
                            <img src="<?php echo base_url() ?>assets/img/client-logo/paketku.png" alt="clients">
                        </a>
                    </div>
                </div>
                 <div class="col-xs-12 col-md-4 col-sm-6">
                    <div class="border-box">
                        <a href="#">
                            <img src="<?php echo base_url() ?>assets/img/client-logo/mnd.jpg" alt="clients">
                        </a>
                    </div>
                </div>
                 <div class="col-xs-12 col-md-4 col-sm-6">
                    <div class="border-box">
                        <a href="#">
                          <img src="<?php echo base_url() ?>assets/img/client-logo/ayana.png" alt="clients">
                        </a>
                    </div>
                </div>
                 <div class="col-xs-12 col-md-6 col-sm-6">
                    <div class="border-box">
                        <a href="#">
                            <img src="<?php echo base_url() ?>assets/img/client-logo/arrow.jpg" alt="clients">
                        </a>
                    </div>
                </div>
                 <div class="col-xs-12 col-md-6 col-sm-6">
                    <div class="border-box">
                        <a href="#">
                            <img src="<?php echo base_url() ?>assets/img/client-logo/courier.png" alt="clients">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

