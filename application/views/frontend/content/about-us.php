<div class="structure">
	
	<div class="tittle" id="tittle-structure">
		<h1>ABOUT US</h1>
		<p>PT. Total Inti Corpora provides a specific portfolio of information technology solutions and business process related to logistics and supply chain management.</p>
	</div>
<section class="section-about">
	<div class="container">
		<div class="row">
		<div class="border-bottom-tab">

			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#tab-1" role="tab" class="waves-effect waves-dark" data-toggle="tab">About us</a></li>
				<li role="presentation"><a href="#tab-2" role="tab" class="waves-effect waves-dark" data-toggle="tab">What We Do</a></li>
				<li role="presentation"><a href="#tab-3" role="tab" class="waves-effect waves-dark" data-toggle="tab">Our Mission</a></li>
				<li role="presentation"><a href="#tab-4" role="tab" class="waves-effect waves-dark" data-toggle="tab">Setps</a></li>
			</ul>

			<div class="panel-body">
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane fade in active" id="tab-1">
						<img class="alignright" src="<?= base_url() ?>assets/img/seo/tab-1.png" alt="">
						<p style="text-align: justify;">Duis senectus mus condimentum nunc ac habitasse duis consectetur a arcu a accumsan cras et metus ultricies justo cum a bibendum. <a href="#">Egestas vestibulum blandit sem vestibulum curabitur</a> a vel aliquet gravida ullamcorper amet dictumst vestibulum a elementum proin id volutpat magna parturient. Id ac dui libero a ullamcorper euismod himenaeos a nam condimentum a adipiscing dapibus lobortis iaculis morbi.</p>
						<p style="text-align: justify;">Himenaeos a vestibulum morbi. <a href="#">Ullamcorper cras scelerisque</a> taciti lorem metus feugiat est lacinia facilisis id nam leo condimentum praesent id diam. Vestibulum amet porta odio elementum convallis parturient tempor tortor tempus a mi ad parturient ad nulla mus amet in penatibus nascetur at vulputate euismod a est tristique scelerisque. Aliquet facilisis nisl vel vestibulum dignissim gravida ullamcorper hac quisque ad at nisl egestas adipiscing vel blandit.</p>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="tab-2">
						<img class="alignright" src="<?= base_url() ?>assets/img/seo/tab-2.png" alt="">
						<p style="text-align: justify;">Duis senectus mus condimentum nunc ac habitasse duis consectetur a arcu a accumsan cras et metus ultricies justo cum a bibendum. <a href="#">Egestas vestibulum blandit sem vestibulum curabitur</a> a vel aliquet gravida ullamcorper amet dictumst vestibulum a elementum proin id volutpat magna parturient. Id ac dui libero a ullamcorper euismod himenaeos a nam condimentum a adipiscing dapibus lobortis iaculis morbi.</p>
						<p style="text-align: justify;">Himenaeos a vestibulum morbi. <a href="#">Ullamcorper cras scelerisque</a> taciti lorem metus feugiat est lacinia facilisis id nam leo condimentum praesent id diam. Vestibulum amet porta odio elementum convallis parturient tempor tortor tempus a mi ad parturient ad nulla mus amet in penatibus nascetur at vulputate euismod a est tristique scelerisque. Aliquet facilisis nisl vel vestibulum dignissim gravida ullamcorper hac quisque ad at nisl egestas adipiscing vel blandit.</p>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="tab-3">
						<img class="alignright" src="<?= base_url() ?>assets/img/seo/tab-3.png" alt="">
						<p style="text-align: justify;">Duis senectus mus condimentum nunc ac habitasse duis consectetur a arcu a accumsan cras et metus ultricies justo cum a bibendum. <a href="#">Egestas vestibulum blandit sem vestibulum curabitur</a> a vel aliquet gravida ullamcorper amet dictumst vestibulum a elementum proin id volutpat magna parturient. Id ac dui libero a ullamcorper euismod himenaeos a nam condimentum a adipiscing dapibus lobortis iaculis morbi.</p>
						<p style="text-align: justify;">Himenaeos a vestibulum morbi. <a href="#">Ullamcorper cras scelerisque</a> taciti lorem metus feugiat est lacinia facilisis id nam leo condimentum praesent id diam. Vestibulum amet porta odio elementum convallis parturient tempor tortor tempus a mi ad parturient ad nulla mus amet in penatibus nascetur at vulputate euismod a est tristique scelerisque. Aliquet facilisis nisl vel vestibulum dignissim gravida ullamcorper hac quisque ad at nisl egestas adipiscing vel blandit.</p>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="tab-4">
						<img class="alignright" src="<?= base_url() ?>assets/img/seo/tab-4.png" alt="">
						<p style="text-align: justify;">Duis senectus mus condimentum nunc ac habitasse duis consectetur a arcu a accumsan cras et metus ultricies justo cum a bibendum. <a href="#">Egestas vestibulum blandit sem vestibulum curabitur</a> a vel aliquet gravida ullamcorper amet dictumst vestibulum a elementum proin id volutpat magna parturient. Id ac dui libero a ullamcorper euismod himenaeos a nam condimentum a adipiscing dapibus lobortis iaculis morbi.</p>
						<p style="text-align: justify;">Himenaeos a vestibulum morbi. <a href="#">Ullamcorper cras scelerisque</a> taciti lorem metus feugiat est lacinia facilisis id nam leo condimentum praesent id diam. Vestibulum amet porta odio elementum convallis parturient tempor tortor tempus a mi ad parturient ad nulla mus amet in penatibus nascetur at vulputate euismod a est tristique scelerisque. Aliquet facilisis nisl vel vestibulum dignissim gravida ullamcorper hac quisque ad at nisl egestas adipiscing vel blandit.</p>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</section>

		<div class="row" id="structure">
			
			<div class="col-sm-12 col-md-12 col-lg-12" id="bagian">
				<h2><strong>Structure and Staff</strong></h1>
					<div class="pimpinan">
						<img src="<?= base_url() ?>assets/img/logoatas.png" style="width: 200px;"> 
							<p><strong>Established 2013</strong></p>
						<div class="co-founder">
						<img src="<?= base_url() ?>assets/img/foto1.png" style="width: 200px;" class="rounded-image	">
							<p>Co-Founder<p>
							<h3>Mr. Boyke Sutendi</h3>
						</div>
					</div>
				<p class="total-staff"><strong>Total Staff</strong></p>
				<div class="row">
					<div class="icon-staff">
					<div class="col-md-4" id="staff">
						<img src="<?= base_url() ?>assets/img/icon-programer.png" style="width: 150px;">
						<p>3 Core Programmer</p>
					</div>
					<div class="col-md-4" id="staff">
						<img src="<?= base_url() ?>assets/img/icon-tester.png" style="width: 165px;">
						<p>5 Tester / Programmer / Documenter</p>
					</div>
					<div class="col-md-4" id="staff">
						<img src="<?= base_url() ?>assets/img/icon-design.ico" style="width: 150px;">
						<p>2 Design Analyst</p>
					</div>
					</div>
				</div>
				<!-- <p><strong>Technology Expertise</strong></p>
				<p>• Web base programming ( HTML5, javascript, PHP, JSON, XML )</p>
				<p>• Databases (oracle, mysql, firebase, sqlite, postgresql )</p>
				<p>• FrontEnd/Server Side programming ( perl,python, java, pascal, .net)</p>
				<p>• Mobile Apps</p>
				<p>• Linux, Windows, Android, IOS platforms</p> -->
			</div>
		</div>
	</div>