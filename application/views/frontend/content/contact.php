

<section class="section-padding" id="contact">
	<div class="container">
		<div class="text-center mb-50 mt-40">
			<h2 class="section-title text-uppercase">Contact Us</h2>
		</div>
		<div class="row">
			<div class="col-md-8">
				<form name="contact-form" id="contactForm" action="http://trendytheme.net/demo/matrox/sendemail.php" method="POST">
					<div class="row">
						<div class="col-md-6">
							<div class="input-field">
								<input type="text" name="name" class="validate" id="name">
								<label for="name">Name</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-field">
								<label class="sr-only" for="email">Email</label>
								<input id="email" type="email" name="email" class="validate">
								<label for="email" data-error="wrong" data-success="right">Email</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-field">
								<input id="phone" type="tel" name="phone" class="validate">
								<label for="phone">Phone Number</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-field">
								<input id="website" type="text" name="website" class="validate">
								<label for="website">Your Website</label>
							</div>
						</div>
					</div>
					<div class="input-field">
						<textarea name="message" id="message" class="materialize-textarea"></textarea>
						<label for="message">Message</label>
					</div>
					<button type="submit" name="submit" class="waves-effect waves-light btn submit-button  mt-30">Send Message</button>
				</form>
			</div>
			<div class="col-md-4 contact-info">
				<address>
					<i class="material-icons brand-color">&#xE55F;</i>
					<div class="address">
						<p> Maisonette Mega Kebon Jeruk,
						Unit 12 & 15,
						Jl.Raya Joglo Jakarta Barat 11640
					</p>	
					</div>
					<i class="material-icons brand-color">&#xE61C;</i>
					<div class="phone">
					<p> Fax:  021 - 5890 8525<br>
						Phone: 021 - 5858 276</p>
					</div>
					<i class="material-icons brand-color">&#xE0E1;</i>
					<div class="mail">
						<p><a href="mailto:#">boyke@tora.co.id</a><br>
						</div>
					</address>
				</div>
			</div>
		</div>
	</section>

<!-- 
	<div id="myMap" class="height-350"></div> -->


