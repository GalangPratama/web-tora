<div class="solution">
	<div class="tittle" id="tittle-solution">
		<h1>solution</h1>
		<p>PT. Total Inti Corpora provides a specific portfolio of information technology
		solutions and business process related to logistics and supply chain management. </p>
	</div>
	<div id="solution">
			<section class="section-padding">
				<div class="text-center mb-40" id="desc">
					<h2 class="section-title text-uppercase" style="color: #55ACEE;">SOLUTION</h2>
					<p class="section-sub">PT. TOTAL INTI CORPORA provides a specific portfolio of information technology solutions and business process related to logistics and supply chain management. </p>
				</div>
				<div class="row" id="row-solution">
					<div class="icon-solution">
						<div class="col-md-2" id="image-solution">
							<img src="<?= base_url() ?>assets/img/icon/icon-warehouse.png" style="width: 150px;">
							<p>Warehouse Management System</p>
						</div>
						<div class="col-md-2" id="image-solution">
							<img src="<?= base_url() ?>assets/img/icon/icon-partmanagement.png" style="width: 150px;">
							<p>Part Management System</p>
						</div>
						<div class="col-md-2" id="image-solution">
							<img src="<?= base_url() ?>assets/img/icon/icon-courirlink.png" style="width: 150px;">
							<p>Courier Link System</p>
						</div>
						<div class="col-md-2" id="image-solution">
							<img src="<?= base_url() ?>assets/img/icon/icon-human.jpg" style="width: 150px;">
							<p>Human Resource Management</p>
						</div>
						<div class="col-md-2" id="image-solution">
							<img src="<?= base_url() ?>assets/img/icon/icon-finance.png" style="width: 150px;">
							<p>Finance Accounting image-solution</p>
						</div>
					</div>
				</div>
			</section>
	</div>
</div>