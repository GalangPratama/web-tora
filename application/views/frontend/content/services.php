<div class="services">
	<div class="tittle" id="tittle-services">
		<h1>services</h1>
		<p>PT. Total Inti Corpora provides a specific portfolio of information technology
			servicess and business process related to logistics and supply chain management. </p>
	</div>
	<div class="container" id="services">
		<div class="row">
			<section class="section-padding">
	<div class="container">
		<div class="text-center mb-80">
			<h2 class="section-title text-uppercase">What we do</h2>
			<p class="section-sub">Quisque non erat mi. Etiam congue et augue sed tempus. Aenean sed ipsum luctus, scelerisque ipsum nec, iaculis justo. Sed at vestibulum purus, sit amet viverra diam. Nulla ac nisi rhoncus,</p>
		</div>
		<div class="row equal-height-row">
			<div class="col-md-4 mb-30">
				<div class="featured-item hover-outline brand-hover radius-4 equal-height-column">
					<div class="icon">
						<i class="material-icons colored brand-icon">&#xE3B7;</i>
					</div>
					<div class="desc">
						<h2>Creative Design</h2>
						<p>Porttitor communicate pandemic data rather than enabled niche markets neque rather than enabled niche markets neque pulvinar.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4 mb-30">
				<div class="featured-item hover-outline brand-hover radius-4 equal-height-column">
					<div class="icon">
						<i class="material-icons colored brand-icon">&#xE326;</i>
					</div>
					<div class="desc">
						<h2>Responsive Design</h2>
						<p>Porttitor communicate pandemic data rather than enabled niche markets neque rather than enabled niche markets neque pulvinar.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4 mb-30">
				<div class="featured-item hover-outline brand-hover radius-4 equal-height-column">
					<div class="icon">
						<i class="material-icons colored brand-icon">&#xE8B8;</i>
					</div>
					<div class="desc">
						<h2>Flexible Page Builder</h2>
						<p>Porttitor communicate pandemic data rather than enabled niche markets neque rather than enabled niche markets neque pulvinar.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="featured-item hover-outline brand-hover radius-4 equal-height-column">
					<div class="icon">
						<i class="material-icons colored brand-icon">&#xE325;</i>
					</div>
					<div class="desc">
						<h2>Mobile Applicaion Design</h2>
						<p>Porttitor communicate pandemic data rather than enabled niche markets neque rather than enabled niche markets neque pulvinar.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="featured-item hover-outline brand-hover radius-4 equal-height-column">
					<div class="icon">
						<i class="material-icons colored brand-icon">&#xE3B0;</i>
					</div>
					<div class="desc">
						<h2>Professional Photography</h2>
						<p>Porttitor communicate pandemic data rather than enabled niche markets neque rather than enabled niche markets neque pulvinar.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="featured-item hover-outline brand-hover radius-4 equal-height-column">
					<div class="icon">
						<i class="material-icons colored brand-icon">&#xE62E;</i>
					</div>
					<div class="desc">
						<h2>Moting Graphics Design</h2>
						<p>Porttitor communicate pandemic data rather than enabled niche markets neque rather than enabled niche markets neque pulvinar.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="section-padding banner-10 bg-fixed parallax-bg overlay light-9" data-stellar-background-ratio="0.5">
<div class="container">
<div class="text-center mb-80">
<h2 class="section-title text-uppercase">Techonologis we use</h2>
<p class="section-sub">Quisque non erat mi. Etiam congue et augue sed tempus. Aenean sed ipsum luctus, scelerisque ipsum nec, iaculis justo. Sed at vestibulum purus, sit amet viverra diam nulla ac nisi rhoncus.</p>
</div>
<div class="featured-carousel brand-dot">
<div class="featured-item border-box radius-4 hover brand-hover">
<div class="icon mb-30">
<img src="assets/img/anguler-logo.png" alt="">
</div>
<div class="desc">
<h2>Angular JS</h2>
<p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets neque pulvinar vitae.</p>
</div>
</div>
<div class="featured-item border-box radius-4 hover brand-hover">
<div class="icon mb-30">
<img src="assets/img/node-logo.png" alt="">
</div>
<div class="desc">
<h2>Node JS</h2>
<p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets neque pulvinar vitae.</p>
</div>
</div>
<div class="featured-item border-box radius-4 hover brand-hover">
<div class="icon mb-30">
<img src="assets/img/material-logo.png" alt="">
</div>
<div class="desc">
<h2>materialize CSS</h2>
<p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets neque pulvinar vitae.</p>
</div>
</div>
<div class="featured-item border-box radius-4 hover brand-hover">
<div class="icon mb-30">
<img src="assets/img/node-logo.png" alt="">
</div>
<div class="desc">
<h2>We are creative</h2>
<p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets neque pulvinar vitae.</p>
</div>
</div>
<div class="featured-item border-box radius-4 hover brand-hover">
<div class="icon mb-30">
<img src="assets/img/anguler-logo.png" alt="">
</div>
<div class="desc">
<h2>We are creative</h2>
<p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets neque pulvinar vitae.</p>
</div>
</div>
<div class="featured-item border-box radius-4 hover brand-hover">
<div class="icon mb-30">
<img src="assets/img/material-logo.png" alt="">
</div>
<div class="desc">
<h2>We are creative</h2>
<p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets neque pulvinar vitae.</p>
</div>
</div>
<div class="featured-item border-box radius-4 hover brand-hover">
<div class="icon mb-30">
<img src="assets/img/node-logo.png" alt="">
</div>
<div class="desc">
<h2>We are creative</h2>
<p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets neque pulvinar vitae.</p>
</div>
</div>
<div class="featured-item border-box radius-4 hover brand-hover">
<div class="icon mb-30">
<img src="assets/img/material-logo.png" alt="">
</div>
<div class="desc">
<h2>We are creative</h2>
<p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets neque pulvinar vitae.</p>
</div>
</div>
</div>
</div>
</section>
		</div>
	</div>
</div>