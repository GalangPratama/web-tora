<footer class="footer footer-four">
    <div class="primary-footer dark-bg lighten-4 text-center">
        <div class="container">
            <a href="#top" class="page-scroll btn-floating btn-large back-top waves-effect waves-light" data-section="#top">
                <i class="material-icons">&#xE316;</i>
            </a>
            <ul class="social-link tt-animate ltr mt-20">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-tumblr"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-rss"></i></a></li>
            </ul>
            <hr class="mt-15">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="footer-logo">
                        <img src="<?php echo base_url() ?>assets/img/logo-white.png" alt="">
                    </div>
                    <div class="footer-intro">
                        <p>PT. TOTAL INTI CORPORA provides a specific portfolio of information technology solutions and business process related to logistics and supply chain management. Our core portfolio comprises application development, business process consulting services as well as professional staffing services in information technology.</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3" id="footer-company">
                    <div class="footer-company">
                        <h2>COMPANY</h2>
                    </div>
                    <div class="footer-menu">
                        <ul>
                            <li><a href="<?= base_url('dashboard')?>">Home</a></li>
                            <li><a href="<?= base_url('service')?>">Service</a></li>
                            <li><a href="<?= base_url('career')?>">Careers</a></li>
                            <li><a href="<?= base_url('contact')?>">Contact</a></li>
                        </ul>  
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3" id="footer-address">
                    <div class="footer-address">
                       <h2>OFFICE ADDRESS</h2>
                    </div>
                    <div class="footer-adr">
                        <p>Maisonette, Mega Kebon Jeruk, Unit 12 & 15,
                            Jl.Raya Joglo Jakarta Barat 11640
                        </p>
                        <p>
                            <a href="#">Phone: Fax : 021 - 5890 8525</a><br>
                        
                            <a href="#">Email: boyke@tora.co.id</a> <br>
                        
                            <a href="#">web: www.tora.co.id</a><br>
                        </p>
    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="secondary-footer dark-bg lighten-3 text-center">
        <div class="container">
            <ul>
                <li><a href="#">© Copyright 2019 Development by Total Inti Corpora</a></li>
            </ul>
        </div>
    </div>
</footer>