<div id="preloader">
    <div class="preloader-position">
        <img src="<?php echo base_url() ?>assets/img/logo-colored.png" alt="logo">
        <div class="progress">
            <div class="indeterminate"></div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>assets/js/jquery-2.1.3.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/script.js"></script>
<script src="<?php echo base_url() ?>assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/materialize/js/materialize.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/smoothscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/menuzord.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap-tabcollapse.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.inview.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.countTo.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/imagesloaded.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.shuffle.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.stellar.min.js"></script>
<script src="<?php echo base_url() ?>assets/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url() ?>assets/owl.carousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/scriptsae52.js?v=5"></script>

<script src="<?php echo base_url() ?>assets/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo base_url() ?>assets/revolution/js/jquery.themepunch.revolution.min.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery(".materialize-slider").revolution({
            sliderType: "standard",
            sliderLayout: "fullwidth",
            delay: 9000,
            navigation: {
                keyboardNavigation: "on",
                keyboard_direction: "horizontal",
                mouseScrollNavigation: "off",
                onHoverStop: "off",
                touch: {
                    touchenabled: "on",
                    swipe_threshold: 75,
                    swipe_min_touches: 1,
                    swipe_direction: "horizontal",
                    drag_block_vertical: false
                },
                arrows: {
                    style: "gyges",
                    enable: true,
                    hide_onmobile: false,
                    hide_onleave: true,
                    tmp: '',
                    left: {
                        h_align: "left",
                        v_align: "center",
                        h_offset: 10,
                        v_offset: 0
                    },
                    right: {
                        h_align: "right",
                        v_align: "center",
                        h_offset: 10,
                        v_offset: 0
                    }
                }
            },
            responsiveLevels: [1240, 1024, 778, 480],
            gridwidth: [1140, 1024, 778, 480],
            gridheight: [700, 600, 800, 800],
            disableProgressBar: "on",
            parallax: {
                type: "mouse",
                origo: "slidercenter",
                speed: 2000,
                levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50],
            }

        });
    });
</script>

<script type="text/javascript" src="<?php echo base_url() ?>assets/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js"></script>
 -->
		<!-- Dynamic JQuery -->
		      <?php if(isset($includes_for_layout['js']) AND count($includes_for_layout['js']) > 0): ?>
		      <?php foreach($includes_for_layout['js'] as $js): ?>
		        <?php if($js['options'] === NULL OR $js['options'] == 'footer'): ?>
		          <script type="text/javascript" src="<?php echo $js['file']; ?>"></script>
		        <?php endif; ?>
		      <?php endforeach; ?>
		    <?php endif; ?>

		    <script type="text/javascript">
		      <?php echo $init ?>
		    </script>
				 <!-- Dynamic JQuery -->
		      <?php if(isset($includes_for_layout['js']) AND count($includes_for_layout['js']) > 0): ?>
		      <?php foreach($includes_for_layout['js'] as $js): ?>
		        <?php if($js['options'] === NULL OR $js['options'] == 'footer'): ?>
		          <script type="text/javascript" src="<?php echo $js['file']; ?>"></script>
		        <?php endif; ?>
		      <?php endforeach; ?>
		    <?php endif; ?>

		    <script type="text/javascript">
		      <?php echo $init ?>
		    </script>
	</body>
</html>