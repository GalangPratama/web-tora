<!doctype html>
<html lang="en-US">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Total Inti Corpora">
    <meta name="keywords" content="agency, stratup, corporate, business, technlogy, creative, information">
    <meta name="author" content="agisrh">
    <title>PT. Total Inti Corpora - Think Synergy</title>

    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url() ?>assets/img/ico/icon.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url() ?>assets/img/ico/icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url() ?>assets/img/ico/icon.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>assets/img/ico/icon.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url() ?>assets/img/ico/icon.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url() ?>assets/img/ico/icon.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url() ?>assets/img/ico/icon.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url() ?>assets/img/ico/icon.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url() ?>assets/img/ico/icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url() ?>assets/img/ico/icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url() ?>assets/img/ico/icon.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>assets/img/ico/icon.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo base_url() ?>assets/img/ico/ms-icon-144x144.png">

    <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,700|Raleway:100,200,300,400,500,600,700,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,400,500,600,700|Open+Sans:300,400,600,700|Playfair+Display:400,700&display=swap" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/fonts/iconfont/material-icons.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/owl.carousel/assets/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/owl.carousel/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/materialize/css/materialize.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/shortcodes/shortcodes.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/skins/tora.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/skins/seo.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/revolution/css/settings.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/revolution/css/navigation.css">

    <!-- Dynamic Stylesheet -->
    <?php if(isset($includes_for_layout['css']) AND count($includes_for_layout['css']) > 0): ?>
        <?php foreach($includes_for_layout['css'] as $css): ?>
            <link rel="stylesheet" type="text/css" href="<?php echo $css['file']; ?>" <?php echo ($css[ 'options']===NULL ? '' : ' media="' . $css[ 'options'] . '"'); ?>>
            <?php endforeach; ?>
                <?php endif; ?>

                    <?php if(isset($includes_for_layout['js']) AND count($includes_for_layout['js']) > 0): ?>
                        <?php foreach($includes_for_layout['js'] as $js): ?>
                            <?php if($js['options'] !== NULL AND $js['options'] == 'header'): ?>
                                <script type="text/javascript" src="<?php echo $js['file']; ?>"></script>
                                <?php endif; ?>
                                    <?php endforeach; ?>
                                        <?php endif; ?>

</head>