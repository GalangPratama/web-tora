<body id="top" class="has-header-search">

   <!--  <div class="top-bar dark-bg lighten-2 visible-md visible-lg">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="list-inline social-top tt-animate btt">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-tumblr"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-rss"></i></a></li>
                    </ul>
                </div>
                <div class="col-md-6 text-right">
                    <ul class="topbar-cta no-margin">
                        <li class="mr-20">
                            <a><i class="material-icons mr-10">&#xE0B9;</i>boyke@tora.co.id</a>
                        </li>
                        <li>
                            <a><i class="material-icons mr-10">&#xE0CD;</i> 021 - 5858 276</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div> -->

    <header id="header" class="tt-nav">
        <div class="header-sticky light-header">
            <div class="container">
                <div id="materialize-menu" class="menuzord">
                    <a href="<?= base_url('dashboard')?>" class="logo-brand">
                        <img src="<?php echo base_url() ?>assets/img/logoatas.png" alt="">
                    </a>
                    <a href="<?= base_url('dashboard')?>" class="logo-brand-sticky">
                        <img src="<?php echo base_url() ?>assets/img/tora123.png" alt="">
                    </a>
                    <ul class="menuzord-menu pull-right">
                        <li><a href="<?= base_url('dashboard')?>">Home</a></li>
                        <li><a href="<?= base_url('service')?>">Services</a></li>
                        <li><a href="<?= base_url('solution')?>">Solution</a></li>
                        <li><a href="<?= base_url('about_us')?>">About Us</a></li>
                        <li><a href="<?= base_url('vision')?>">Vision</a></li>
                        <li><a href="<?= base_url('contact')?>">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>    
    </header>