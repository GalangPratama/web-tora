
<?php $this->load->view('frontend/layout/Header'); ?>
<?php $this->load->view('frontend/layout/Navbar'); ?>
<div class="content-container no-padding">
	<div class="container-full">
			<div class="row">
				<div class="col-xs-12">
					<div class="main-content">
						<?php $this->load->view($contents); ?>
					</div>
				</div>
			</div>
	</div>
</div>
<?php $this->load->view('frontend/layout/Footer'); ?>
<?php $this->load->view('frontend/layout/Scripts'); ?>
