<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class vision extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','language','parse'));

	}

	public function index()
	{
		$data['contents']="frontend/content/vision";
		$this->load->view('frontend/Mainlayout', $data);
	}

	
}
