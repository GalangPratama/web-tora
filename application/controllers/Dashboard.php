<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','language','parse'));

	}

	public function index()
	{
		$data['contents']="frontend/Dashboard";
		$this->load->view('frontend/Mainlayout', $data);
	}
}
