<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class solution extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','language','parse'));

	}

	public function index()
	{
		$data['contents']="frontend/content/solution";
		$this->load->view('frontend/Mainlayout', $data);
	}
}
